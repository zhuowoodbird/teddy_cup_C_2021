# -*- coding: utf-8 -*-
from pyhanlp import *
import numpy as np
from gensim import corpora
import pandas as pd
from solve_q1 import stopwords_remove, string_seg
from sklearn.cluster import DBSCAN
from gensim.corpora import Dictionary
from sklearn.feature_extraction import DictVectorizer
import matplotlib.pyplot as plt

CRFSegmenter = JClass('com.hankcs.hanlp.model.crf.CRFSegmenter')
CRFLexicalAnalyzer = JClass('com.hankcs.hanlp.model.crf.CRFLexicalAnalyzer')

def feature_extract(X):
    dictionary = corpora.Dictionary(X)    
    # 将数据转换为稀疏矩阵
    corpus = [dictionary.doc2bow(text) for text in X]    
    
    data = []
    for i in corpus:
        data.append(dict(i))   # 把tuple 转换为 dict
    
    vec = DictVectorizer(sparse=True, dtype=np.uint8)    # 真正转换成可处理的矩阵
    data_new = vec.fit_transform(data)
    return data_new

def clustering(X, eps=0.5, min_samples=2):
    '''
    DBSCAN 聚类
    '''
    db = DBSCAN(eps=eps, min_samples=2, n_jobs=-1)
    db.fit(X)
    labels = db.labels_
    return labels
    
def cluster_param_select(X, eps_begin=0.1, eps_end=2,
                        strides=0.1, verbose=True):
    '''
    筛选 DBSCAN 聚类模型的参数
    '''
    # 聚类簇列表、游离个体数列表
    num_clu_list = []
    num_indi_list = []
    # 产生迭代参数
    epsilon_list = np.arange(eps_begin, eps_end, strides)
    for eps in epsilon_list:
        labels = clustering(X, eps)
        # 聚类簇
        clusters = np.unique(labels)
        # 聚类簇数量
        num_clusters = len(clusters)
        # 游离个体数量
        individuals = list(filter(lambda x:x==-1, labels))
        # create list
        num_clu_list.append(num_clusters)
        num_indi_list.append(len(individuals))
    
    if verbose:
        # 画图
        plt.figure(figsize=(12, 4))
        star_point = 8
        star_point_2 = 9
        plt.subplots_adjust(left=0.125, bottom=None, right=0.9, top=None,
                wspace=0.3, hspace=None)
        plt.subplot(1,2,1)
        
        plt.plot(epsilon_list, num_clu_list) 
        plt.plot(epsilon_list[star_point], num_clu_list[star_point], 
                marker='*', markersize=12)
        plt.plot(epsilon_list[star_point_2], num_clu_list[star_point_2], 
                marker='*', markersize=12)                
        plt.xlabel(u'epsilon',fontsize=20)
        plt.ylabel(u'number of clusters',fontsize=20)

        plt.subplot(1,2,2)
        plt.plot(epsilon_list, num_indi_list) 
        plt.plot(epsilon_list[star_point], num_indi_list[star_point], 
        marker='*', markersize=12)
        plt.plot(epsilon_list[star_point_2], num_indi_list[star_point_2], 
        marker='*', markersize=12)        
        plt.xlabel(u'epsilon',fontsize=20)
        plt.ylabel(u'number of individuals',fontsize=20)  
        plt.show()
    
    return epsilon_list[star_point], epsilon_list[star_point_2]
    

def test_param(data_raw, labels, rank=1):
    '''
    查看某个聚类下的效果
    '''
    X = data_raw.loc[data_raw['景区名称']=='A01']
    X['所属簇'] = labels
    label_num = dict()
    labels_set = set(np.unique(labels)) 

    for label in labels_set:
        X_tmp = X.loc[X['所属簇']==label]['评论内容']
        label_num[label] = label_num.get(label, 0) + X_tmp.shape[0] 
    
    # 输出聚类簇-样品数
    print('聚类簇-样品数： ', label_num) 
    
    # 根据值排序
    label_num_sort = sorted(label_num.items(), key=lambda x: x[1], reverse=True)

    label_num_max = label_num_sort[rank][0] 
    # 输出内容
    X_tmp = X.loc[X['所属簇']==label_num_max]['评论内容'].values
    print('样品数最多的聚类下，内容: \n', X_tmp)
    
def return_valid_result(data, labels):
    '''
    返回有效评论：
    保留游离数据，
    从聚类簇中筛选最长的一句作为有效数据代表。
    '''
    label_num = dict()
    labels_set = set(np.unique(labels)) 
    data['所属簇'] = labels
    # 构建空白变量
    columns = data.columns
    data_valid = pd.DataFrame(columns=columns)
    for label in labels_set: 
        # 游离数据不参与筛选
        if label == -1:
            df_valid_indiv = data.loc[data['所属簇']==-1]
            data_valid = pd.concat([data_valid, df_valid_indiv])
            continue
        # 保留最长
        df_valid_selection = data.loc[data['所属簇']==label]
        df_tmp = df_valid_selection.copy()
        df_tmp = df_tmp['评论内容']
        # 找出最长
        max_len = df_tmp.apply(lambda x: len(x)).max()
        
        df_valid_selection = df_valid_selection.loc[df_valid_selection['评论内容'] \
                                                    .apply(lambda x:len(x)) \
                                                    ==max_len]
        if df_valid_selection.shape[0] > 1:
            df_valid_selection = df_valid_selection.sample(n=1)
            
        data_valid = pd.concat([data_valid, df_valid_selection])
    
    return data_valid

if  __name__ == '__main__':
    comment_file_path = r'../附件/景区评论.xlsx'
    crf_model_path = r'../附件/model_crf.txt' 
    data_raw = pd.read_excel(comment_file_path)
    X = data_raw.loc[data_raw['景区名称']=='A01']['评论内容']
    X = list(X)
    # 分词
    crf_model_path = r'../附件/model_crf.txt'     
    X_seg, _ = string_seg(X, crf_model_path)
    # 停用词过滤
    X_remove = stopwords_remove(X_seg, [])

    # 词频词袋模型
    X_new = feature_extract(X_remove)
    # 筛选聚类参数
    eps_1, eps_2 = cluster_param_select(X_new)
    
    # 
    input('按任意键继续')
    # 结果比较 
    labels_1 = clustering(X_new, eps_1)
    labels_2 = clustering(X_new, eps_2)
    test_param(data_raw, labels_1)
    # 输出样品最多，第二多的聚类簇和内容
    test_param(data_raw, labels_2)
    test_param(data_raw, labels_2)
    
    # 输出结果
    data = data_raw.loc[data_raw['景区名称']=='A01']
    data_valid = return_valid_result(data, labels_2)
    data_valid.drop(['所属簇'], axis=1, inplace=True)
    data_valid.to_excel(r'../附件/问题三结果_景区A01.xlsx')
