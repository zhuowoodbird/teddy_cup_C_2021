# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from pyhanlp import *
from gensim.models import TfidfModel
from gensim.corpora import Dictionary
import warnings
warnings.filterwarnings('ignore')

CRFSegmenter = JClass('com.hankcs.hanlp.model.crf.CRFSegmenter')
CRFLexicalAnalyzer = JClass('com.hankcs.hanlp.model.crf.CRFLexicalAnalyzer')

def string_seg(X, crf_model_path):
    HanLP.Config.ShowTermNature = False    #不显示词性
    segmenter = CRFSegmenter(crf_model_path)    #加载模型
    segment = CRFLexicalAnalyzer(segmenter)    #实例化一个条件随机场分词器
    segment.enableCustomDictionary(False)    #集成用户字典
    segment.enablePartOfSpeechTagging(True)    #分词启动数字、英文不拆分
    segment.enablePlaceRecognize(True)     # 开启地名识别
    segment.enableOrganizationRecognize(True)    #开启组织名识别
    segment.enableNameRecognize(True)    # 开启名字识别
    
    # 应用条件随机场分词
    tokens = []
    for string in X:
        token = segment.seg(string)
        tokens.append(token)
        # tokens 是一个列表，包含多个单词
    return tokens, segment

def stopwords_remove(tokens, additional_stopwords):
    # 加载停用词字典，并实例化一个停用词过滤对象
    stopwords_filter = JClass('com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary')()   
    if additional_stopwords:
        for word in additional_stopwords:
           stopwords_filter.add(word)
           
    # 通过停用词字典，用双向匹配过滤掉所有停用词
    tokens_stopword = []
    for string_token in tokens:
        #过滤停用词（必须先进行分词）
        string_token = stopwords_filter.apply(string_token)   
        tmp = []
        for i in string_token:
            tmp.append(str(i))
        tokens_stopword.append(tmp)
    
    return tokens_stopword    

def extract_keywords(comments, keywords_num):
    '''
    提取热词
    '''
    comments_join = ''
    for comment in comments:
        # 拼接成长文本
        comments_join += comment
    # 返回关键词
    keyword_list = HanLP.extractKeyword(comments_join, keywords_num)
    keyword_list = list(keyword_list)
    # 过滤掉 **
    try:
        keyword_list.remove('**')
    except:
        pass
    
    return keyword_list

def return_buzzwords(comments_remove, keywords_list):
    '''
    计算热度
    '''
    # 词频词袋模型
    dct = Dictionary(comments_remove)
    corpus = [dct.doc2bow(text) for text in comments_remove]
    # tf-idf 模型
    model = TfidfModel(corpus)
    # 关键词转数字（index）
    keywords_list = dct.doc2idx(keywords_list)
    # 热度计算
    hotness = dict()
    for freq_list in corpus:
        # 单词（列表）的 TF-IDF
        tf_idf = model[freq_list]
        for n in range(len(freq_list)):
            # 若不在关键词表内，不参与热词排行
            if freq_list[n][0] not in keywords_list:
                continue
            # 热度 = TF-IDF + 词频
            hotness[freq_list[n][0]] = hotness.get(freq_list[n][0], 0) + \
                                        freq_list[n][1] + tf_idf[n][1]
    # 按热度排序，并取出前 20 位
    hotness_set = sorted(hotness.items(), key=lambda x: x[1], reverse=True)
    hotness_set = hotness_set[:20]
    
    terms = []
    hotnesses = []
    for term_idx, hotness in hotness_set:
        term = dct.get(term_idx)
        terms.append(term)
        hotnesses.append(hotness)

    df = pd.DataFrame.from_dict({'热门词':terms, '热度':hotnesses})
    return df

def return_top_buzzwords(comments, crf_model_path, verbose=True):
    '''
    返回 20 个热门词
    '''
    # 对评论进行拆分
    comments_seg, crf_model = string_seg(comments, crf_model_path)   
    # 停用词、无意义词过滤。
    additional_stopwords = ['\t', '\n', '我', '我们', '你', '你们', '他们', '他',
                        '太', '带', '挺', '超', '**', '*', '***', '里面', '没', '排'
                        , '说', '里']
    
    comments_remove = stopwords_remove(comments_seg, additional_stopwords)
    
    # 关键词提取
    num = 100
    keywords_list = extract_keywords(comments, num)
    
    if verbose:
        print('100 个关键词为： ', keywords_list)
    
    # 返回热门词
    df = return_buzzwords(comments_remove, keywords_list)
    
    return df



if __name__ == '__main__':
    crf_model_path = r'../附件/model_crf.txt' 
    file_path = r'../附件/景区评论.xlsx'
    # 读取数据表格：景区评论
    data_raw = pd.read_excel(file_path)
    # 针对一个酒店的评论内容
    comments = data_raw.loc[data_raw['景区名称']=='A01']['评论内容']
    comments = list(comments)
    
    # 返回热门词，并输出 excel
    df = return_top_buzzwords(comments, crf_model_path)
    df['景点'] = 'A01'
    xlsx_file = r'../附件/热门词词云.xlsx'
    df.to_excel(xlsx_file)


