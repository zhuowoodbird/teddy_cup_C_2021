# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from data_preprocessing import join_comments
from pyhanlp import *
import pickle
from solve_q1 import return_top_buzzwords

CRFSegmenter = JClass('com.hankcs.hanlp.model.crf.CRFSegmenter')
CRFLexicalAnalyzer = JClass('com.hankcs.hanlp.model.crf.CRFLexicalAnalyzer')


def extract_keysentences(comment_text):
    '''
    提取出 100 条关键句，并组合成文本
    '''
    key_sentence = list(HanLP.extractSummary(comment_text, 100))

    return key_sentence

if __name__ == '__main__':
    # 加载文件
    crf_model_path = r'../附件/model_crf.txt' 
    
    score_file_path = r'../附件/景区评分.xlsx'
    scores = pd.read_excel(score_file_path)

    comment_file_path = r'../附件/景区评论.xlsx'
    data_raw = pd.read_excel(comment_file_path)

    total_score = scores['总得分']

    #  计算分位数
    q1 = total_score.quantile(q=0.25)
    q3 = total_score.quantile(q=0.75)

    # join
    total_score = total_score.to_frame()
    total_score['景区名称'] = scores['景区名称'].values
    data_with_score = data_raw.merge(total_score, how='inner', 
                                    left_on='景区名称',
                                    right_on='景区名称')
    
    # 选出高、中、低三种
    high_end = data_with_score.loc[data_with_score['总得分']>q3].sample(frac=0.1, replace=False)
    low_end = data_with_score.loc[data_with_score['总得分']<=q1].sample(frac=0.1, replace=False)
    modest = data_with_score.loc[(data_with_score['总得分']<=q3) & 
                                (data_with_score['总得分']>q1)].sample(frac=0.1, replace=False)
    
    # 拼接
    modest_comments = modest['评论内容']
    modest_comments = join_comments(modest_comments)
    high_end_comments = high_end['评论内容']
    high_end_comments = join_comments(high_end_comments)
    low_end_comments = low_end['评论内容']
    low_end_comments = join_comments(low_end_comments)
    
    # 提取关键句，并保存
    high_text = extract_keysentences(high_end_comments)
    modest_text = extract_keysentences(modest_comments)
    low_end_text = extract_keysentences(low_end_comments)

    # 高
    buzzwords_high = return_top_buzzwords(high_text, 
                                          crf_model_path, 
                                          verbose=False)
    xlsx_file = r'../附件/热门词词云_高层次.xlsx'
    buzzwords_high.to_excel(xlsx_file)

    # 中
    buzzwords_modest = return_top_buzzwords(modest_text, 
                                          crf_model_path, 
                                          verbose=False)
    xlsx_file = r'../附件/热门词词云_较好层次.xlsx'
    buzzwords_modest.to_excel(xlsx_file)

    # 低端
    buzzwords_low = return_top_buzzwords(low_end_text, 
                                          crf_model_path, 
                                          verbose=False)
    xlsx_file = r'../附件/热门词词云_一般层次.xlsx'
    buzzwords_low.to_excel(xlsx_file)
