# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
from pyhanlp import *
import pickle
from sklearn.feature_extraction import DictVectorizer
from gensim.models import TfidfModel
from gensim import corpora 
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import warnings
warnings.filterwarnings('ignore')
BigramTokenizer = JClass('com.hankcs.hanlp.classification.tokenizers.BigramTokenizer')


def join_comments(comments):
    '''
    将一个景区的评论结合成一个长文档
    '''
    comments_join = ''
    for comment in comments:
        comments_join += comment
    # 返回长文档
    return comments_join
    
def feature_extract(structured_data_list):
    '''
    将数据转换为 TF-IDF 矩阵
    '''
    dictionary = corpora.Dictionary(structured_data_list)    
    # 将数据转换为词频词袋模型
    corpus = [dictionary.doc2bow(ngram) for ngram in structured_data_list]    
    model = TfidfModel(corpus)
    # 构建 TF-IDF 临时变量
    tf_idf_data = []
    for freq_list in corpus:
        # 计算每一个景区评论长文本的 TF-IDF
        tf_idf_list = model[freq_list]
        # 把tuple 转换为 dict
        tf_idf_data.append([tf_idf[1] for tf_idf in tf_idf_list]) 

    # 将数据转换为 np.array
    length = max(map(len, tf_idf_data))
    tf_idf_matrix = np.array([tf_idf_list+[0] \
                    *(length-len(tf_idf_list)) \
                    for tf_idf_list in tf_idf_data])
    
    
    return tf_idf_matrix


def return_structure_data(data_raw):
    '''
    输入原始数据，将数据转换为结构化的 TF-IDF 表格
    '''
    # 得到所有景区的名称
    scenic_spots = list(data_raw['景区名称'].unique())
    # 构建一个所有进去结构化后的临时变量
    structured_data = []
    # 对每一个景区
    for spot in scenic_spots:
        comments = data_raw.loc[data_raw['景区名称']==spot]['评论内容']
        # 将多条评论组合成一个长文档
        comments = join_comments(comments)
        
        # 二元语法拆分 （2-grams tokenizer）    
        tokenizer = BigramTokenizer()
        comments_tokens = tokenizer.segment(comments)
        
        structured_data.append(comments_tokens)
       
    structured_data_list = []
    for i in range(len(structured_data)):
        # 因为经过 hanlp 二元语法拆分后，返回的是一个 Java 字符串类型，所以需要进行人工转换成list
        # 否则，直接采用 .to_list 会导致，数据类型为 Java。
        structured_data_list.append(list(structured_data[i]))
                        
    data_matrix = feature_extract(structured_data_list)
    return data_matrix

def decomposition_pca(matrix, n_components=50, verbose=False):
    pca = PCA(n_components=n_components)
    pca.fit(matrix)
    if verbose:
        # 画图
        plt.subplots_adjust(left=0.125, bottom=None, right=0.9, top=None,
                wspace=0.3, hspace=None)
        plt.subplot(1,2,1)
        evrs = pca.explained_variance_ratio_
        n_components = range(1, len(evrs)+1)
        plt.bar(n_components, evrs)   #画出贡献图
        plt.xlabel(u'component',fontsize=20)
        plt.ylabel(u'evr',fontsize=20)

        evr_sum = evrs.copy()
        for i in range(1,len(evrs)):
            evr_sum[i] = evr_sum[i-1]+evr_sum[i]
            
        plt.subplot(1,2,2)
        plt.bar(n_components, evr_sum)   #画出贡献图
        plt.xlabel(u'component',fontsize=20)
        plt.ylabel(u'comsum_evr',fontsize=20)  
        plt.show()
    
    matrix_pca = pca.transform(matrix)
    return matrix_pca

if __name__ == '__main__':
    comment_file_path = r'../附件/景区评论.xlsx'
    data_raw = pd.read_excel(comment_file_path)
    
    # 数据预处理
    data_matrix = return_structure_data(data_raw)
    # 保存数据
    #pickle.dump(data_matrix, open(r'../附件/TF-IDF数据.pkl', 'wb'))
    matrix_pca = decomposition_pca(data_matrix)
    
    # 数据标准化
    scaler = StandardScaler()
    matrix_std = scaler.fit_transform(matrix_pca)
    # 保存数据
    pickle.dump(matrix_std, open(r'../附件/data_for_q2.pkl', 'wb'))
